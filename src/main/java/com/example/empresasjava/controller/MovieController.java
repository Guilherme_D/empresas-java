package com.example.empresasjava.controller;

import com.example.empresasjava.models.Movie;
import com.example.empresasjava.models.MovieRating;
import com.example.empresasjava.models.RequestEntity.MovieRatingRequest;
import com.example.empresasjava.models.RequestEntity.MovieRequest;
import com.example.empresasjava.models.RequestEntity.UserRequest;
import com.example.empresasjava.models.ResponseEntity.MovieResponse;
import com.example.empresasjava.models.ResponseEntity.UserResponse;
import com.example.empresasjava.models.User;
import com.example.empresasjava.service.MovieService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/movie")
public class MovieController {

    @Autowired
    MovieService movieService;

    @PostMapping(path = "/create")
    @ApiOperation(value = "Criar novo filme")
    public ResponseEntity<Movie> createUser(
            @ApiParam(value = "Json da requisição que contem o dado do filme a ser salvo")
            @Valid @RequestBody MovieRequest request){

        return ResponseEntity.ok().body(
                this.movieService.create(request)
        );
    }

    @PostMapping(path = "/vote")
    @ApiOperation(value = "Votar em um filme")
    public ResponseEntity<MovieRating> createUser(
            @ApiParam(value = "Json da requisição que contem o voto")
            @Valid @RequestBody MovieRatingRequest request){

        return ResponseEntity.ok().body(
                this.movieService.vote(request)
        );
    }

    @GetMapping(path = "/page/{page}/size/{size}filter/{filter}/value/{value}/orderby/{orderBy}")
    @ResponseBody
    @ApiOperation(value = "Lista filmes por página e quantidade")
    public List<MovieResponse> listUsersByPageWithSize(
            @ApiParam(value = "Página que deseja visualizar iniciando em 0", example = "0")
            @PathVariable(value="page")
                    int page,
            @ApiParam(value = "Quantidade de filmes a serem listados por página", example = "10")
            @PathVariable(value="size")
                    int size,
            @ApiParam(value = "Filtro a ser utilizado", example = "title")
            @PathVariable(value="filter")
                    String filter,
            @ApiParam(value = "valor do filtro", example = "string")
            @PathVariable(value="value")
                    String value,
            @ApiParam(value = "Ordenar por média de votos (votes) ou ordem alfabetica (title)", example = "votos")
            @PathVariable(value="orderBy")
            String orderBy){

        Pageable pages = PageRequest.of(page, size);
        return this.movieService.listMovieByFilterAndPage(pages, filter, value, orderBy);
    }

    @GetMapping(path = "/movie/{id}")
    @ResponseBody
    @ApiOperation(value = "Mostrar detalhes de um filme pelo id")
    public ResponseEntity<MovieResponse> listUsersByPageWithSize(
            @ApiParam(value = "Id do filme que deseja buscar", example = "0")
            @PathVariable(value="id")//aqui, mandando vazio erro 404 no message
                    int id){

        return ResponseEntity.ok().body(
                this.movieService.getMovieDetailsById(id)
        );
    }

    @DeleteMapping(path = "/delete/{id}")
    @ApiOperation(value = "Desativa usuário existente")
    public ResponseEntity<Movie> deleteUser(@PathVariable(value="id") final Integer id){
        return ResponseEntity.ok().body(
                this.movieService.deleteMovie(id)
        );
    }
}
