package com.example.empresasjava.repository;

import com.example.empresasjava.models.Types;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypesRepository  extends JpaRepository<Types,Integer> {
    List<Types> findByTypesIn(List<String> types);
}
