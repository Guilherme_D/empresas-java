package com.example.empresasjava.repository;

import com.example.empresasjava.models.PG;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PGRepository extends JpaRepository<PG,Integer> {
    PG findOneByPg(String pg);
}
