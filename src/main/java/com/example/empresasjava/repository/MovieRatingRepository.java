package com.example.empresasjava.repository;

import com.example.empresasjava.models.MovieRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRatingRepository extends JpaRepository<MovieRating,Integer> {
    MovieRating findByUserIdAndMovieId(Integer userId, Integer movieId);
}
