package com.example.empresasjava.models;

import com.google.common.base.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @NotNull(message = "Campo title não pode ser nulo")
    @NotEmpty(message = "Campo title não pode ser vazio")
    private String title;
    @NotNull(message = "Campo description não pode ser nulo")
    @NotEmpty(message = "Campo description não pode ser vazio")
    private String description;
    @NotNull(message = "Campo directors não pode ser nulo")
    @NotEmpty(message = "Campo directors não pode ser vazio")
    private String directors;
    @NotNull(message = "Campo actors não pode ser nulo")
    @NotEmpty(message = "Campo actors não pode ser vazio")
    private String actors;


    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "vote" )
    private List<MovieRating> vote;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "pg")
    private PG pg;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "types")
    private List<Types> types;

    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;

    public Movie(){}

    public Movie(@NotNull(message = "Campo title não pode ser nulo")
                 @NotEmpty(message = "Campo title não pode ser vazio")
                 String title,
                 @NotNull(message = "Campo description não pode ser nulo")
                 @NotEmpty(message = "Campo description não pode ser vazio")
                 String description,
                 @NotNull(message = "Campo directors não pode ser nulo")
                 @NotEmpty(message = "Campo directors não pode ser vazio")
                 String directors,
                 @NotNull(message = "Campo actors não pode ser nulo")
                 @NotEmpty(message = "Campo actors não pode ser vazio")
                 String actors,
                 List<MovieRating> vote,
                 PG pg,
                 List<Types> movie_type) {

        this.title = title;
        this.description = description;
        this.directors = directors;
        this.actors = actors;
        this.vote = vote;
        this.pg = pg;
        this.types = movie_type;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public List<MovieRating> getvote() {
        return vote;
    }

    public void setvote(List<MovieRating> vote) {
        this.vote = vote;
    }

    public PG getPg() {
        return pg;
    }

    public void setPg(PG pg) {
        this.pg = pg;
    }

    public List<Types> getTypes() {
        return types;
    }

    public void setTypes(List<Types> types) {
        this.types = types;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equal(id, movie.id) && Objects.equal(title, movie.title) && Objects.equal(description, movie.description) && Objects.equal(directors, movie.directors) && Objects.equal(actors, movie.actors) && Objects.equal(vote, movie.vote) && Objects.equal(pg, movie.pg) && Objects.equal(types, movie.types) && Objects.equal(createdAt, movie.createdAt) && Objects.equal(deletedAt, movie.deletedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, title, description, directors, actors, vote, pg, types, createdAt, deletedAt);
    }

}
