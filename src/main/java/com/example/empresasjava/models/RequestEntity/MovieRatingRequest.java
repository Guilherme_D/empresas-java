package com.example.empresasjava.models.RequestEntity;

import com.google.common.base.Objects;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class MovieRatingRequest {
    @NotNull(message = "Campo title não pode ser nulo")
    @NotEmpty(message = "Campo title não pode ser vazio")
    String title;
    @NotNull(message = "Campo vote não pode ser nulo")
    @Max(value = 4, message = "O voto deve ser de 0 a 4")
    @Min(value = 0, message = "O voto deve ser de 0 a 4")
    Integer vote;

    MovieRatingRequest(){}

    public MovieRatingRequest(
            @NotNull(message = "Campo título não pode ser nulo")
            @NotEmpty(message = "Campo título não pode ser vazio")
            String title,
            @NotNull(message = "Campo voto não pode ser nulo")
            @Max(value = 4, message = "O voto deve ser de 0 a 4")
            @Min(value = 0, message = "O voto deve ser de 0 a 4")
            Integer vote) {
        this.title = title;
        this.vote = vote;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRatingRequest that = (MovieRatingRequest) o;
        return Objects.equal(title, that.title) && Objects.equal(vote, that.vote);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(title, vote);
    }
}
