package com.example.empresasjava.models.RequestEntity;

import com.example.empresasjava.models.Movie;
import com.example.empresasjava.models.MovieType;
import com.example.empresasjava.models.PG;
import com.example.empresasjava.models.Types;
import com.google.common.base.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class MovieRequest {

    @NotNull(message = "Campo title não pode ser nulo")
    @NotEmpty(message = "Campo title não pode ser vazio")
    private String title;
    @NotNull(message = "Campo description não pode ser nulo")
    @NotEmpty(message = "Campo description não pode ser vazio")
    private String description;
    @NotNull(message = "Campo directors não pode ser nulo")
    @NotEmpty(message = "Campo directors não pode ser vazio")
    private String directors;
    @NotNull(message = "Campo actors não pode ser nulo")
    @NotEmpty(message = "Campo actors não pode ser vazio")
    private String actors;
    @NotNull(message = "Campo pg não pode ser nulo")
    @NotEmpty(message = "Campo pg não pode ser vazio")
    private String pg;

    private Integer vote;
    private List<String> types;

    public MovieRequest(){}

    public MovieRequest(
            @NotNull(message = "Campo title não pode ser nulo")
            @NotEmpty(message = "Campo title não pode ser vazio")
            String title,
            @NotNull(message = "Campo description não pode ser nulo")
            @NotEmpty(message = "Campo decription não pode ser vazio")
            String description,
            @NotNull(message = "Campo directors não pode ser nulo")
            @NotEmpty(message = "Campo directors não pode ser vazio")
            String directors,
            @NotNull(message = "Campo actors não pode ser nulo")
            @NotEmpty(message = "Campo actors não pode ser vazio")
            String actors,
            @NotNull(message = "Campo pg não pode ser nulo")
            @NotEmpty(message = "Campo pg não pode ser vazio")
            String pg,
            Integer vote,
            List<String> types
    ) {
        this.title = title;
        this.description = description;
        this.directors = directors;
        this.actors = actors;
        this.vote = vote;
        this.pg = pg;
        this.types = types;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public Integer getvote() {
        return vote;
    }

    public void setvote(Integer vote) {
        this.vote = vote;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public static Movie toMovie(MovieRequest movieRequest){
        PG g = new PG(movieRequest.getPg());

        List<Types> ts = new ArrayList<>();
        List<String> types = movieRequest.getTypes();

        types.forEach(t -> {ts.add(new Types(t));});

        MovieType mt = new MovieType();

        Movie movie = new Movie();

        movie.setTitle(movieRequest.getTitle());
        movie.setDescription(movieRequest.getDescription());
        movie.setDirectors(movieRequest.getDirectors());
        movie.setActors(movieRequest.getActors());
        movie.setPg(g);
        movie.setTypes(ts);

        return movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRequest that = (MovieRequest) o;
        return Objects.equal(title, that.title) && Objects.equal(description, that.description) && Objects.equal(directors, that.directors) && Objects.equal(actors, that.actors);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(title, description, directors, actors);
    }
}
