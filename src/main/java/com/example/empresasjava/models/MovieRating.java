package com.example.empresasjava.models;

import com.google.common.base.Objects;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "movie_rating")
public class MovieRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo MovieId não pode ser nulo")
    Integer movieId;

    @NotNull(message = "Campo UserId não pode ser nulo")
    Integer userId;

    @NotNull(message = "Campo Vote não pode ser nulo")
    @Max(value = 4, message = "O voto deve ser de 0 a 4")
    @Min(value = 0, message = "O voto deve ser de 0 a 4")
    Integer vote;

    public MovieRating(){}

    public MovieRating(@NotNull(message = "Campo movieId não pode ser nulo")
                       Integer movieId,
                       @NotNull(message = "Campo userId não pode ser nulo")
                       Integer userId,
                       @NotNull(message = "Campo vote não pode ser nulo")
                       Integer vote) {
        this.movieId = movieId;
        this.userId = userId;
        this.vote = vote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieRating that = (MovieRating) o;
        return Objects.equal(id, that.id) && Objects.equal(movieId, that.movieId) && Objects.equal(userId, that.userId) && Objects.equal(vote, that.vote);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, movieId, userId, vote);
    }

}
