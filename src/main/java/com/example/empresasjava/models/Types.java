package com.example.empresasjava.models;

import com.google.common.base.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "types")
public class Types {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo types não pode ser nulo")
    @NotEmpty(message = "Campo types não pode ser vazio")
    String types;

    public Types(){}

    public Types(@NotNull(message = "Campo types não pode ser nulo") @NotEmpty(message = "Campo types não pode ser vazio") String types) {
        this.types = types;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Types types1 = (Types) o;
        return Objects.equal(id, types1.id) && Objects.equal(types, types1.types);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, types);
    }
}
