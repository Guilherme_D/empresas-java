package com.example.empresasjava.models;

import com.google.common.base.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "movies_types")
public class MovieType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo moviesId não pode ser nulo")
    @NotEmpty(message = "Campo moviesId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id" )
    Movie moviesId;

    @NotNull(message = "Campo typesId não pode ser nulo")
    @NotEmpty(message = "Campo typesId não pode ser vazio")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "types_id" )
    Types typesId;

    public MovieType(){}

    public MovieType(@NotNull(message = "Campo moviesId não pode ser nulo")
                     @NotEmpty(message = "Campo moviesId não pode ser vazio")
                             Movie moviesId,
                     @NotNull(message = "Campo typesId não pode ser nulo")
                     @NotEmpty(message = "Campo typesId não pode ser vazio")
                             Types typesId) {
        this.moviesId = moviesId;
        this.typesId = typesId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie getMoviesId() {
        return moviesId;
    }

    public void setMoviesId(Movie moviesId) {
        this.moviesId = moviesId;
    }

    public Types getTypesId() {
        return typesId;
    }

    public void setTypesId(Types typesId) {
        this.typesId = typesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieType movieType = (MovieType) o;
        return Objects.equal(id, movieType.id) && Objects.equal(moviesId, movieType.moviesId) && Objects.equal(typesId, movieType.typesId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, moviesId, typesId);
    }
}
