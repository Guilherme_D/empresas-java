package com.example.empresasjava.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "pg")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PG {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Campo pg não pode ser nulo")
    @NotEmpty(message = "Campo pg não pode ser vazio")
    String pg;

    public PG(){}

    public PG(@NotNull(message = "Campo pg não pode ser nulo") @NotEmpty(message = "Campo pg não pode ser vazio") String pg) {
        this.pg = pg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPg() {
        return pg;
    }

    public void setPg(String pg) {
        this.pg = pg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PG pg1 = (PG) o;
        return Objects.equal(id, pg1.id) && Objects.equal(pg, pg1.pg);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, pg);
    }
}
