package com.example.empresasjava.service.impl;

import com.example.empresasjava.models.MovieRating;
import com.example.empresasjava.models.RequestEntity.MovieRatingRequest;
import com.example.empresasjava.repository.MovieRatingRepository;
import com.example.empresasjava.service.MovieRatingService;
import com.example.empresasjava.service.MovieService;
import com.example.empresasjava.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MovieRatingServiceImpl implements MovieRatingService {

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private UserService userService;
    @Override
    public MovieRating create(MovieRatingRequest request) {


        Integer movieId = this.movieService.findByTitle(request.getTitle()).getId();
        Integer userId = this.userService.getUserByPrincipal().getId();



        Optional<MovieRating> mr = Optional.ofNullable(this.movieRatingRepository.findByUserIdAndMovieId(userId, movieId));

        if(!mr.isPresent()){
                MovieRating movieRating = new MovieRating(
                        movieId,
                        userId,
                        request.getVote()
                );
                return this.movieRatingRepository.save(movieRating);
        }else{
            mr.get().setVote(request.getVote());
            return this.movieRatingRepository.save(mr.get());
        }



    }
}
