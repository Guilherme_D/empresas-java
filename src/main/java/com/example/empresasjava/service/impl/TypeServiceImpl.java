package com.example.empresasjava.service.impl;

import com.example.empresasjava.models.Types;
import com.example.empresasjava.repository.TypesRepository;
import com.example.empresasjava.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypesRepository typesRepository;

    @Override
    public List<Types> create(List<String> types) {

        List<String> typesRequested = new ArrayList<>(types);
        List<Types> byTypesIn = this.typesRepository.findByTypesIn(types);

        byTypesIn.forEach(a -> typesRequested.remove(a.getTypes()));

        List<Types> types_to_save = new ArrayList<>();

        typesRequested.forEach(t -> types_to_save.add(new Types(t)));

        return this.typesRepository.saveAll(types_to_save);
    }

    @Override
    public List<Types> findByTypesIn(List<String> types) {
        return this.typesRepository.findByTypesIn(types);
    }
}
