package com.example.empresasjava.service.impl;

import com.example.empresasjava.models.PG;
import com.example.empresasjava.repository.PGRepository;
import com.example.empresasjava.service.PGService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PGServiceImpl implements PGService {

    @Autowired
    private PGRepository pgRepository;

    @Override
    public PG findOneByPg(String pg) {
        return this.pgRepository.findOneByPg(pg);
    }

    @Override
    public PG create(PG pg) {
        return this.pgRepository.save(pg);
    }
}
