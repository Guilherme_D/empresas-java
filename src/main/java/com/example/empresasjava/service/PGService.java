package com.example.empresasjava.service;

import com.example.empresasjava.models.PG;

public interface PGService {
    PG findOneByPg(String pg);
    PG create(PG pg);
}
