package com.example.empresasjava.service;

import com.example.empresasjava.models.Movie;
import com.example.empresasjava.models.MovieRating;
import com.example.empresasjava.models.RequestEntity.MovieRatingRequest;
import com.example.empresasjava.models.RequestEntity.MovieRequest;
import com.example.empresasjava.models.ResponseEntity.MovieResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface MovieService {
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie create(MovieRequest movieRequest);
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie deleteMovie(Integer id);
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    Movie editMovie(MovieRequest movieRequest);

    @PreAuthorize("!@authorityChecker.isAllowed(authentication)")
    MovieRating vote(MovieRatingRequest movie);

    Movie findByTitle(String title);

    List<MovieResponse> listMovieByFilterAndPage(Pageable page, String filter, String value, String Order_by);

    MovieResponse getMovieDetailsById(Integer id);
}
