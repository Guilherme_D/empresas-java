package com.example.empresasjava.service;

import com.example.empresasjava.models.MovieRating;
import com.example.empresasjava.models.RequestEntity.MovieRatingRequest;

public interface MovieRatingService {
    MovieRating create(MovieRatingRequest request);
}
