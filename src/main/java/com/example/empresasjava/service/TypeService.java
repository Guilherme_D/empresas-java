package com.example.empresasjava.service;

import com.example.empresasjava.models.Types;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface TypeService {
    @PreAuthorize("@authorityChecker.isAllowed(authentication)")
    List<Types> create(List<String> type);

    List<Types> findByTypesIn(List<String> types);
}
